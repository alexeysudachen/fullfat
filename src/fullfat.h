/************************************************************************************
 *        FullFAT - High Performance, Thread-Safe Embedded FAT File-System          *
 *                                                                                  *
 *          Copyright(C) 2009  James Walmsley  <james@fullfat-fs.co.uk>             *
 *          Copyright(C) 2011  Hein Tibosch    <hein_tibosch@yahoo.es>              *
 *                                                                                  *
 *       See RESTRICTIONS.TXT for extra restrictions on the use of FullFAT.         *
 *                                                                                  *
 *      THIS SOFTWARE IS FREE FOR COMMERICIAL USE UNDER A LIBERAL BSD LICENSE       *
 *                                                                                  *
 ************************************************************************************
 *               See http://www.fullfat-fs.co.uk/ for more information.             *
 ************************************************************************************
 * Redistribution and use in source and binary forms, with or without               *
 * modification, are permitted provided that the following conditions are met:      *
 *                                                                                  *
 * 1. Redistributions of source code must retain the above copyright notice, this   *
 *    list of conditions and the following disclaimer.                              *
 * 2. Redistributions in binary form must reproduce the above copyright notice,     *
 *    this list of conditions and the following disclaimer in the documentation     *
 *    and/or other materials provided with the distribution.                        *
 * 3. Developers should adhere to the basic fair usage-rules as set out at:         *
 *    http://bitthunder.org/docs/usage-rules/                                       *
 *                                                                                  *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND  *
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED    *
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE           *
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR  *
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES   *
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;     *
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND      *
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT       *
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS    *
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.                     *
 *                                                                                  *
 * The views and conclusions contained in the software and documentation are those  *
 * of the authors and should not be interpreted as representing official policies,  *
 * either expressed or implied, of the BitThunder Project.                          *
 ************************************************************************************/

#ifndef _FULLFAT_H_
#define _FULLFAT_H_

#ifdef	__cplusplus
extern "C" {
#endif

#define FF_VERSION		"2.0.0 (" __DATE__ " : "__TIME__")" 		// The official version number for this release.
#define FF_REVISION		"FullFAT-2.0.0-RTM"							// The official version control commit code for this release.

#include "ff_config.h"
#include "ff_ioman.h"
#include "ff_crc.h"
#include "ff_dir.h"
#include "ff_error.h"
#include "ff_fat.h"
#include "ff_file.h"
#include "ff_hash.h"
#include "ff_time.h"
#include "ff_string.h"
#include "ff_types.h"
#include "ff_unicode.h"
#include "ff_format.h"

#ifdef	__cplusplus
}	// extern "C"
#endif

#endif
