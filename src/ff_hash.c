/************************************************************************************
 *        FullFAT - High Performance, Thread-Safe Embedded FAT File-System          *
 *                                                                                  *
 *          Copyright(C) 2009  James Walmsley  <james@fullfat-fs.co.uk>             *
 *          Copyright(C) 2011  Hein Tibosch    <hein_tibosch@yahoo.es>              *
 *                                                                                  *
 *       See RESTRICTIONS.TXT for extra restrictions on the use of FullFAT.         *
 *                                                                                  *
 *      THIS SOFTWARE IS FREE FOR COMMERICIAL USE UNDER A LIBERAL BSD LICENSE       *
 *                                                                                  *
 ************************************************************************************
 *               See http://www.fullfat-fs.co.uk/ for more information.             *
 ************************************************************************************
 * Redistribution and use in source and binary forms, with or without               *
 * modification, are permitted provided that the following conditions are met:      *
 *                                                                                  *
 * 1. Redistributions of source code must retain the above copyright notice, this   *
 *    list of conditions and the following disclaimer.                              *
 * 2. Redistributions in binary form must reproduce the above copyright notice,     *
 *    this list of conditions and the following disclaimer in the documentation     *
 *    and/or other materials provided with the distribution.                        *
 * 3. Developers should adhere to the basic fair usage-rules as set out at:         *
 *    http://bitthunder.org/docs/usage-rules/                                       *
 *                                                                                  *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND  *
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED    *
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE           *
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR  *
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES   *
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;     *
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND      *
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT       *
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS    *
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.                     *
 *                                                                                  *
 * The views and conclusions contained in the software and documentation are those  *
 * of the authors and should not be interpreted as representing official policies,  *
 * either expressed or implied, of the BitThunder Project.                          *
 ************************************************************************************/

/**
 *	@file		ff_hash.c
 *	@author		James Walmsley
 *	@ingroup	HASH
 *
 *	@defgroup	HASH HASH Table
 *	@brief		Provides a simple HASH lookup table.
 *
 **/

#include "ff_hash.h"
#include <stdlib.h>
#include <string.h>

#ifdef FF_HASH_CACHE
struct _FF_HASH_TABLE {
	FF_T_UINT8 bitTable[FF_HASH_TABLE_SIZE];
};

/**
 *
 *
 **/
FF_HASH_TABLE FF_CreateHashTable() {
	FF_HASH_TABLE pHash = (FF_HASH_TABLE) FF_MALLOC(sizeof(struct _FF_HASH_TABLE));

	if(pHash) {
		FF_ClearHashTable(pHash);
		return pHash;
	}

	return NULL;
}

FF_ERROR FF_ClearHashTable(FF_HASH_TABLE pHash) {
	if(pHash) {
		memset(pHash->bitTable, 0, FF_HASH_TABLE_SIZE);
		return FF_ERR_NONE;
	}

	return FF_ERR_NULL_POINTER | FF_CLEARHASHTABLE;
}

FF_ERROR FF_SetHash(FF_HASH_TABLE pHash, FF_T_UINT32 nHash) {
	FF_T_UINT32 tblIndex	= ((nHash / 8) % FF_HASH_TABLE_SIZE);
	FF_T_UINT32 tblBit		= nHash % 8;

	if(pHash) {
		pHash->bitTable[tblIndex] |= (0x80 >> tblBit);
		return FF_ERR_NONE;
	}

	return FF_ERR_NULL_POINTER | FF_SETHASH;
}

FF_ERROR FF_ClearHash(FF_HASH_TABLE pHash, FF_T_UINT32 nHash) {
	FF_T_UINT32 tblIndex	= ((nHash / 8) % FF_HASH_TABLE_SIZE);
	FF_T_UINT32 tblBit		= nHash % 8;

	if(pHash) {
		pHash->bitTable[tblIndex] &= ~(0x80 >> tblBit);
		return FF_ERR_NONE;
	}

	return FF_ERR_NULL_POINTER | FF_CLEARHASH;
}

FF_T_BOOL FF_isHashSet(FF_HASH_TABLE pHash, FF_T_UINT32 nHash) {
	FF_T_UINT32 tblIndex	= ((nHash / 8) % FF_HASH_TABLE_SIZE);
	FF_T_UINT32 tblBit		= nHash % 8;

	if(pHash) {
		if(pHash->bitTable[tblIndex] & (0x80 >> tblBit)) {
			return FF_TRUE;
		}
	}
	return FF_FALSE;
}

FF_ERROR FF_DestroyHashTable(FF_HASH_TABLE pHash) {
	if(pHash) {
		FF_FREE(pHash);
		return FF_ERR_NONE;
	}
	return FF_ERR_NULL_POINTER | FF_DESTROYHASHTABLE;
}

#endif
