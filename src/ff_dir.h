/************************************************************************************
 *        FullFAT - High Performance, Thread-Safe Embedded FAT File-System          *
 *                                                                                  *
 *          Copyright(C) 2009  James Walmsley  <james@fullfat-fs.co.uk>             *
 *          Copyright(C) 2011  Hein Tibosch    <hein_tibosch@yahoo.es>              *
 *                                                                                  *
 *       See RESTRICTIONS.TXT for extra restrictions on the use of FullFAT.         *
 *                                                                                  *
 *      THIS SOFTWARE IS FREE FOR COMMERICIAL USE UNDER A LIBERAL BSD LICENSE       *
 *                                                                                  *
 ************************************************************************************
 *               See http://www.fullfat-fs.co.uk/ for more information.             *
 ************************************************************************************
 * Redistribution and use in source and binary forms, with or without               *
 * modification, are permitted provided that the following conditions are met:      *
 *                                                                                  *
 * 1. Redistributions of source code must retain the above copyright notice, this   *
 *    list of conditions and the following disclaimer.                              *
 * 2. Redistributions in binary form must reproduce the above copyright notice,     *
 *    this list of conditions and the following disclaimer in the documentation     *
 *    and/or other materials provided with the distribution.                        *
 * 3. Developers should adhere to the basic fair usage-rules as set out at:         *
 *    http://bitthunder.org/docs/usage-rules/                                       *
 *                                                                                  *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND  *
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED    *
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE           *
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR  *
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES   *
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;     *
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND      *
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT       *
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS    *
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.                     *
 *                                                                                  *
 * The views and conclusions contained in the software and documentation are those  *
 * of the authors and should not be interpreted as representing official policies,  *
 * either expressed or implied, of the BitThunder Project.                          *
 ************************************************************************************/

/**
 *	@file		ff_dir.h
 *	@author		James Walmsley
 *	@ingroup	DIR
 **/
#ifndef _FF_DIR_H_
#define _FF_DIR_H_

#include "ff_types.h"
#include "ff_config.h"
#include "ff_error.h"
#include "ff_ioman.h"
#include "ff_blk.h"
#include "ff_fat.h"
#include "ff_fatdef.h"
#include "ff_memory.h"
#include "ff_time.h"
#include "ff_hash.h"
#include "ff_crc.h"
#include "ff_file.h"
#include <string.h>

typedef struct {
	FF_T_UINT32	ulChainLength;
	FF_T_UINT32	ulDirCluster;
	FF_T_UINT32	ulCurrentClusterLCN;
	FF_T_UINT32	ulCurrentClusterNum;
	FF_BUFFER	*pBuffer;
} FF_FETCH_CONTEXT;

typedef struct {
	FF_T_UINT32 Filesize;
	FF_T_UINT32	ObjectCluster;
	
	// Book Keeping
	FF_T_UINT32	CurrentCluster;
	FF_T_UINT32 AddrCurrentCluster;
	FF_T_UINT32	DirCluster;
	FF_T_UINT16	CurrentItem;
	// End Book Keeping
		
#ifdef FF_TIME_SUPPORT	
	FF_SYSTEMTIME	CreateTime;		///< Date and Time Created.
	FF_SYSTEMTIME	ModifiedTime;	///< Date and Time Modified.
	FF_SYSTEMTIME	AccessedTime;	///< Date of Last Access.
#endif

#ifdef FF_FINDAPI_ALLOW_WILDCARDS
#ifdef FF_UNICODE_SUPPORT
	FF_T_WCHAR	szWildCard[FF_MAX_FILENAME];
#else
	FF_T_INT8	szWildCard[FF_MAX_FILENAME];
#endif
	FF_T_BOOL	bInvertWildCard;
#endif

#ifdef FF_UNICODE_SUPPORT
	FF_T_WCHAR	FileName[FF_MAX_FILENAME];
#else
	FF_T_INT8	FileName[FF_MAX_FILENAME];
#endif

#if defined(FF_LFN_SUPPORT) && defined(FF_INCLUDE_SHORT_NAME)
	FF_T_INT8	ShortName[13];
#endif
	FF_T_UINT8	Attrib;
	FF_FETCH_CONTEXT FetchContext;
} FF_DIRENT;



// PUBLIC API
#ifdef FF_UNICODE_SUPPORT
FF_ERROR	FF_FindFirst		(FF_IOMAN *pIoman, FF_DIRENT *pDirent, const FF_T_WCHAR *path);
FF_ERROR	FF_MkDir			(FF_IOMAN *pIoman, const FF_T_WCHAR *Path);
#else
FF_ERROR	FF_FindFirst		(FF_IOMAN *pIoman, FF_DIRENT *pDirent, const FF_T_INT8 *path);
FF_ERROR	FF_MkDir			(FF_IOMAN *pIoman, const FF_T_INT8 *Path);
#endif

FF_ERROR	FF_FindNext			(FF_IOMAN *pIoman, FF_DIRENT *pDirent);
FF_ERROR	FF_RewindFind		(FF_IOMAN *pIoman, FF_DIRENT *pDirent);

// INTERNAL API
FF_ERROR	FF_GetEntry			(FF_IOMAN *pIoman, FF_T_UINT16 nEntry, FF_T_UINT32 DirCluster, FF_DIRENT *pDirent);
FF_ERROR	FF_PutEntry			(FF_IOMAN *pIoman, FF_T_UINT16 Entry, FF_T_UINT32 DirCluster, FF_DIRENT *pDirent);
FF_T_SINT8	FF_FindEntry		(FF_IOMAN *pIoman, FF_T_UINT32 DirCluster, FF_T_INT8 *Name, FF_DIRENT *pDirent, FF_T_BOOL LFNs);

void		FF_PopulateShortDirent		(FF_IOMAN *pIoman, FF_DIRENT *pDirent, FF_T_UINT8 *EntryBuffer);
FF_ERROR	FF_PopulateLongDirent		(FF_IOMAN *pIoman, FF_DIRENT *pDirent, FF_T_UINT16 nEntry, FF_FETCH_CONTEXT *pFetchContext);
		
FF_ERROR	FF_InitEntryFetch			(FF_IOMAN *pIoman, FF_T_UINT32 ulDirCluster, FF_FETCH_CONTEXT *pContext);
FF_ERROR	FF_FetchEntryWithContext	(FF_IOMAN *pIoman, FF_T_UINT32 ulEntry, FF_FETCH_CONTEXT *pContext, FF_T_UINT8 *pEntryBuffer);
FF_ERROR	FF_PushEntryWithContext		(FF_IOMAN *pIoman, FF_T_UINT32 ulEntry, FF_FETCH_CONTEXT *pContext, FF_T_UINT8 *pEntryBuffer);
FF_ERROR	FF_CleanupEntryFetch		(FF_IOMAN *pIoman, FF_FETCH_CONTEXT *pContext);
		
FF_T_SINT8	FF_PushEntry				(FF_IOMAN *pIoman, FF_T_UINT32 DirCluster, FF_T_UINT16 nEntry, FF_T_UINT8 *buffer, void *pParam);
FF_T_BOOL	FF_isEndOfDir				(FF_T_UINT8 *EntryBuffer);
FF_ERROR	FF_FindNextInDir			(FF_IOMAN *pIoman, FF_DIRENT *pDirent, FF_FETCH_CONTEXT *pFetchContext);

#ifdef FF_UNICODE_SUPPORT
FF_T_UINT32 FF_FindEntryInDir			(FF_IOMAN *pIoman, FF_T_UINT32 DirCluster, const FF_T_WCHAR *name, FF_T_UINT8 pa_Attrib, FF_DIRENT *pDirent, FF_ERROR *pError);
FF_T_SINT32	FF_CreateShortName			(FF_IOMAN *pIoman, FF_T_UINT32 DirCluster, FF_T_WCHAR *ShortName, FF_T_WCHAR *LongName);
#else
FF_T_UINT32 FF_FindEntryInDir			(FF_IOMAN *pIoman, FF_T_UINT32 DirCluster, const FF_T_INT8 *name, FF_T_UINT8 pa_Attrib, FF_DIRENT *pDirent, FF_ERROR *pError);
FF_T_SINT32	FF_CreateShortName			(FF_IOMAN *pIoman, FF_T_UINT32 DirCluster, FF_T_INT8 *ShortName, FF_T_INT8 *LongName);
#endif


void		FF_lockDIR			(FF_IOMAN *pIoman);
void		FF_unlockDIR		(FF_IOMAN *pIoman);

#ifdef FF_UNICODE_SUPPORT
FF_T_UINT32 FF_CreateFile(FF_IOMAN *pIoman, FF_T_UINT32 DirCluster, FF_T_WCHAR *FileName, FF_DIRENT *pDirent, FF_ERROR *pError);
#else
FF_T_UINT32 FF_CreateFile(FF_IOMAN *pIoman, FF_T_UINT32 DirCluster, FF_T_INT8 *FileName, FF_DIRENT *pDirent, FF_ERROR *pError);
#endif

FF_ERROR		FF_CreateDirent		(FF_IOMAN *pIoman, FF_T_UINT32 DirCluster, FF_DIRENT *pDirent);
FF_ERROR		FF_ExtendDirectory	(FF_IOMAN *pIoman, FF_T_UINT32 DirCluster);

#ifdef FF_UNICODE_SUPPORT
FF_T_UINT32		FF_FindDir			(FF_IOMAN *pIoman, const FF_T_WCHAR *path, FF_T_UINT16 pathLen, FF_ERROR *pError);
#else
FF_T_UINT32		FF_FindDir			(FF_IOMAN *pIoman, const FF_T_INT8 *path, FF_T_UINT16 pathLen, FF_ERROR *pError);
#endif

#ifdef FF_HASH_CACHE
FF_T_BOOL FF_CheckDirentHash		(FF_IOMAN *pIoman, FF_T_UINT32 DirCluster, FF_T_UINT32 nHash);
FF_T_BOOL FF_DirHashed				(FF_IOMAN *pIoman, FF_T_UINT32 DirCluster);
FF_ERROR FF_AddDirentHash			(FF_IOMAN *pIoman, FF_T_UINT32 DirCluster, FF_T_UINT32 nHash);
FF_ERROR FF_HashDir					(FF_IOMAN *pIoman, FF_T_UINT32 ulDirCluster);
#endif

FF_ERROR FF_RmLFNs(FF_IOMAN *pIoman, FF_T_UINT16 usDirEntry, FF_FETCH_CONTEXT *pContext);

#endif

