/************************************************************************************
 *        FullFAT - High Performance, Thread-Safe Embedded FAT File-System          *
 *                                                                                  *
 *          Copyright(C) 2009  James Walmsley  <james@fullfat-fs.co.uk>             *
 *          Copyright(C) 2011  Hein Tibosch    <hein_tibosch@yahoo.es>              *
 *                                                                                  *
 *       See RESTRICTIONS.TXT for extra restrictions on the use of FullFAT.         *
 *                                                                                  *
 *      THIS SOFTWARE IS FREE FOR COMMERICIAL USE UNDER A LIBERAL BSD LICENSE       *
 *                                                                                  *
 ************************************************************************************
 *               See http://www.fullfat-fs.co.uk/ for more information.             *
 ************************************************************************************
 * Redistribution and use in source and binary forms, with or without               *
 * modification, are permitted provided that the following conditions are met:      *
 *                                                                                  *
 * 1. Redistributions of source code must retain the above copyright notice, this   *
 *    list of conditions and the following disclaimer.                              *
 * 2. Redistributions in binary form must reproduce the above copyright notice,     *
 *    this list of conditions and the following disclaimer in the documentation     *
 *    and/or other materials provided with the distribution.                        *
 * 3. Developers should adhere to the basic fair usage-rules as set out at:         *
 *    http://bitthunder.org/docs/usage-rules/                                       *
 *                                                                                  *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND  *
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED    *
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE           *
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR  *
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES   *
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;     *
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND      *
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT       *
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS    *
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.                     *
 *                                                                                  *
 * The views and conclusions contained in the software and documentation are those  *
 * of the authors and should not be interpreted as representing official policies,  *
 * either expressed or implied, of the BitThunder Project.                          *
 ************************************************************************************/

/**
 *	@file		ff_blk.c
 *	@author		James Walmsley
 *	@ingroup	BLK
 *
 *	@defgroup	BLK Block Calculater
 *	@brief		Handle Block Number conversions
 *
 *	Helps calculate block numbers.
 **/

#include "ff_blk.h"
/**
 *	@private
 **/
FF_T_UINT32 FF_getClusterChainNumber(FF_IOMAN *pIoman, FF_T_UINT32 nEntry, FF_T_UINT16 nEntrySize) {
	FF_PARTITION *pPart				= pIoman->pPartition;
	FF_T_UINT32 clusterChainNumber	= nEntry / (pIoman->BlkSize * (pPart->SectorsPerCluster * pPart->BlkFactor) / nEntrySize);
	return clusterChainNumber;
}

FF_T_UINT32 FF_getClusterPosition(FF_IOMAN *pIoman, FF_T_UINT32 nEntry, FF_T_UINT16 nEntrySize) {
	return nEntry % ((pIoman->BlkSize * (pIoman->pPartition->SectorsPerCluster * pIoman->pPartition->BlkFactor)) / nEntrySize);
}
/**
 *	@private
 **/
FF_T_UINT32 FF_getMajorBlockNumber(FF_IOMAN *pIoman, FF_T_UINT32 nEntry, FF_T_UINT16 nEntrySize) {
	FF_PARTITION *pPart = pIoman->pPartition;
	FF_T_UINT32 relClusterEntry		= nEntry % (pIoman->BlkSize * (pPart->SectorsPerCluster * pPart->BlkFactor) / nEntrySize);
	FF_T_UINT32 majorBlockNumber	= relClusterEntry / (pPart->BlkSize / nEntrySize);
	return majorBlockNumber;
}
/**
 *	@private
 **/
FF_T_UINT8 FF_getMinorBlockNumber(FF_IOMAN *pIoman, FF_T_UINT32 nEntry, FF_T_UINT16 nEntrySize) {
	FF_PARTITION *pPart				= pIoman->pPartition;
	FF_T_UINT32 relClusterEntry		= nEntry % (pIoman->BlkSize * (pPart->SectorsPerCluster * pPart->BlkFactor) / nEntrySize);
	FF_T_UINT16 relmajorBlockEntry	= (FF_T_UINT16)(relClusterEntry % (pPart->BlkSize / nEntrySize));
	FF_T_UINT8 minorBlockNumber		= (FF_T_UINT8) (relmajorBlockEntry / (pIoman->BlkSize / nEntrySize));
	return minorBlockNumber;
}
/**
 *	@private
 **/
FF_T_UINT32 FF_getMinorBlockEntry(FF_IOMAN *pIoman, FF_T_UINT32 nEntry, FF_T_UINT16 nEntrySize) {
	FF_PARTITION *pPart				= pIoman->pPartition;
	FF_T_UINT32 relClusterEntry		= nEntry % (pIoman->BlkSize * (pPart->SectorsPerCluster * pPart->BlkFactor) / nEntrySize);
	FF_T_UINT32 relmajorBlockEntry	= (FF_T_UINT32)(relClusterEntry % (pPart->BlkSize / nEntrySize));
	FF_T_UINT32 minorBlockEntry		= (FF_T_UINT32)(relmajorBlockEntry % (pIoman->BlkSize / nEntrySize));
	return minorBlockEntry;
}

